# SomethingLike
... tries to recover a password that was ''something like''...

## Disclaimer 1
**!NO, I don't recommend to choose simple passwords that can be recovered so easily!**

## Disclaimer 2
You can pass an arbitrary command to the script. Use at your own risk.

## How to use
```
$ src/somethinglike.py -h
usage: somethinglike.py [-h] [-c CMD] [-p] [-a] [-s SEPARATOR] [-q] W [W ...]

positional arguments:
  W                     a word of the word list

optional arguments:
  -h, --help            show this help message and exit
  -c CMD, --command CMD
                        the command to try the password with
  -p, --permute         permute the word list
  -a, --all             password contains all words
  -s SEPARATOR, --separator SEPARATOR
                        try this word separator
  -q, --quiet           suppress all unnecessary output
```

## Example
You forgot your SSH passphrase, but you remember that it was something like *foo-bar*...
```
$ src/somethinglike.py -c "/usr/bin/openssl rsa -in ~/.ssh/id_rsa -passin stdin" foo bar
foo: Failure
Foo: Failure
bar: Failure
Bar: Failure
foo bar: Failure
foo Bar: Failure
Foo bar: Failure
Foo Bar: Failure
foo-bar: Failure
foo-Bar: Failure
Foo-bar: Failure
Foo-Bar: Failure
foo.bar: Failure
foo.Bar: Failure
Foo.bar: Failure
Foo.Bar: Failure
foo_bar: Failure
foo_Bar: Failure
Foo_bar: Failure
Foo_Bar: Failure
foo*bar: Failure
foo*Bar: Failure
Foo*bar: Failure
Foo*Bar: Failure
foo+bar: Failure
foo+Bar: Failure
Foo+bar: Failure
Foo+Bar: Failure
foobar: Failure
fooBar: Failure
Foobar: Failure
FooBar: Failure
Password not found (or no command given).
```
Or, if you are pretty sure that both *foo* and *bar* must be in the passphrase (`-a`) and that
the separator can only be `#` (`-s '#'`) or `&` (`-s '&'`), but maybe you put *bar* before *foo* (`-p`)...
```
$ src/somethinglike.py -c "/usr/bin/openssl rsa -in ~/.ssh/id_rsa -passin stdin" -a -p -s '#' -s '&' foo bar
foo#bar: Failure
foo#Bar: Failure
Foo#bar: Failure
Foo#Bar: Failure
foo&bar: Failure
foo&Bar: Failure
Foo&bar: Failure
Foo&Bar: Failure
bar#foo: Failure
bar#Foo: Failure
Bar#foo: Failure
Bar#Foo: Failure
bar&foo: Failure
bar&Foo: Failure
Bar&foo: Failure
Bar&Foo: Failure
Password not found.
```

## Copyright
Written by Kathrin Hanauer.

Licensed under GPL version 3.

