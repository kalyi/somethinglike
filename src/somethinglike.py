#!/usr/bin/env python3
# -*- coding: utf8; -*-
#
# Copyright (C) 2019 : Kathrin Hanauer
#
# Try to guess a password consisting of the given words separated
# by spaces/punctuation/...
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

""" Try to guess a password. """

import argparse
import itertools
import subprocess
import shlex
import sys


def tryPassword(command, pw, quiet):
    pwstr = pw + '\n'
    args = shlex.split(command)
    #print(args)
    # throw away output
    proc = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, shell=False)
    # pipe output through
    #proc = subprocess.Popen(args, stdin=subprocess.PIPE, shell=False)
    proc.communicate(input=pwstr.encode('utf-8'))
    proc.wait()
    if not quiet:
        print("{}: {}".format(pw, "Success!" if proc.returncode == 0 else "Failure"))
    return proc.returncode == 0

def swapCases(words):
    cased = []
    for word in words:
        cased += [ (word, word[0].swapcase() + word[1:]) ]
    return cased

def testWords(command, words, separators, replacements, quiet):
    for sep in itertools.combinations_with_replacement(separators, len(words)-1):
        for cwords in itertools.product(*swapCases(words)):
            #print(sep)
            #print(cwords)
            pw = ''.join(itertools.chain.from_iterable(zip(cwords, sep))) + cwords[-1]
            if command and tryPassword(command, pw, quiet):
                return pw
    return None

def guess(command, words, permute, all_words, separators, replacements, quiet):
    if all_words:
        if permute:
            for x in itertools.permutations(words, len(words)):
                pw = testWords(command, x, separators, replacements, quiet)
                if pw:
                    return pw
        else:
            pw = testWords(command, words, separators, replacements, quiet)
            if pw:
                return pw
    else:
        if permute:
            for i in range(0, len(words)):
                for x in itertools.permutations(words, i+1):
                    pw = testWords(command, x, separators, replacements, quiet)
                    if pw:
                        return pw
        else:
            for i in range(0, len(words)):
                for x in itertools.combinations(words, i+1):
                    pw = testWords(command, x, separators, replacements, quiet)
                    if pw:
                        return pw
    return None



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--command", metavar='CMD', required=True, help="the command to try the password with", type=str)
    parser.add_argument("-p", "--permute", action='store_true', help="permute the word list")
    parser.add_argument("-a", "--all", action='store_true', help="password contains all words")
    parser.add_argument("-s", "--separator", action='append', help="try this word separator")
    #parser.add_argument("-r", "--use-replacements", action='store_true', help="use character replacements")
    parser.add_argument("-q", "--quiet", action='store_true', help="suppress all unnecessary output")
    parser.add_argument("words", metavar='W', nargs='+', help="a word of the word list")
    args = parser.parse_args()

    default_separators=[" ", "-", ".", "_", "*", "+", "" ]
    # not implemented yet
    #replacements = {
    #    "a": [ "4" ],
    #    "b": [ "8" ],
    #    "c": [ "k" ],
    #    "d": [ "t" ],
    #    "e": [ "3" ],
    #    "i": [ "1" ],
    #    "k": [ "c" ],
    #    "t": [ "d" ]
    #}
    #pw = guess(args.command, args.words, args.permute, args.all, args.separator if args.separator else default_separators, replacements if args.use_replacements else {}, args.quiet)
    pw = guess(args.command, args.words, args.permute, args.all, args.separator if args.separator else default_separators, {}, args.quiet)
    if pw:
        print("Found password: {}".format(pw))
    else:
        print("Password not found.")
        sys.exit(1)


if __name__ == '__main__':
    main()
